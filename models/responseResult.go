package models

// ResponseResult is the result sent by this backend
type ResponseResult struct {
	Error  string      `json:"error"`
	Result interface{} `json:"result"`
}
