package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Task is the representation of a Todo task
type Task struct {
	ID        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Title     string             `json:"title"`
	Content   string             `json:"content"`
	Alert     time.Time          `json:"alert,omitempty" bson:",omitempty"`
	CreatedAt time.Time          `json:"createdAt"`
	UpdatedAt time.Time          `json:"updatedAt"`
	Done      bool               `json:"done"`

	UserID primitive.ObjectID `json:"user"`
}

// NewTask is the default constructor
func NewTask(task *Task, userID primitive.ObjectID) {

	// instantiate the date values
	task.CreatedAt = time.Now()
	task.UpdatedAt = time.Now()
	task.Done = false
	task.UserID = userID
}
