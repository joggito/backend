package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// User is the representation
type User struct {
	ID         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Identifier string             `json:"identifier"`
	Password   string             `json:"password"`
	Token      string             `json:"token"`
}
