package main

import (
	"etiennepericat/myruns/server/logger"
	"log"
	"net/http"

	routerconf "etiennepericat/myruns/server/config/http"
)

func main() {
	log.Println("=========================")
	log.Println("-- Init My Runs Server --")
	log.Println("=========================")
	router := routerconf.InitializeRouter()
	err := http.ListenAndServe(":8081", router)

	if err != nil {
		logger.LogError("error serving http server", err)
	}
}
