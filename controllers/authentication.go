package controllers

import (
	"context"
	"encoding/json"
	database "etiennepericat/myruns/server/config/database"
	"etiennepericat/myruns/server/logger"
	"etiennepericat/myruns/server/models"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

// PostRegisterUser is used to register a new user in the system
func PostRegisterUser(w http.ResponseWriter, r *http.Request) {
	logger.LogEndpointCall("Register User", "POST")
	w.Header().Set("Content-Type", "application/json")

	// retrieve the user from the request body
	var user models.User
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &user)
	var res models.ResponseResult
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(w).Encode(res)
		return
	}

	// open the users collection
	users := database.GetUsers()

	// check if there is a corresponding user already registered in the DB
	var result models.User
	err = users.FindOne(context.TODO(), bson.D{{"identifier", user.Identifier}}).Decode(&result)

	if err != nil {
		// user not found
		if err.Error() == "mongo: no documents in result" {

			// CREATING a new user
			// encrypt pass
			hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 5)
			if err != nil {
				res.Error = "Error While Hashing Password, Try Again"
				json.NewEncoder(w).Encode(res)
				return
			}

			// Insert the new user
			user.Password = string(hash)
			_, err = users.InsertOne(context.TODO(), user)
			if err != nil {
				res.Error = "Error While Creating User, Try Again"
				json.NewEncoder(w).Encode(res)
				return
			}
			res.Result = "Registration Successful"
			json.NewEncoder(w).Encode(res)
			return
		}

		res.Error = err.Error()
		json.NewEncoder(w).Encode(res)
		return
	}
	// already exists
	res.Result = "identifier already Exists!!"
	json.NewEncoder(w).Encode(res)
	return
}

// PostResetPassword is used to reset a user's password
func PostResetPassword(w http.ResponseWriter, r *http.Request) {
	logger.LogEndpointCall("ResetPassword", "POST")
	w.Header().Set("Content-Type", "application/json")
}

// GetLoginUser is used to Login and authenticate a user
func GetLoginUser(w http.ResponseWriter, r *http.Request) {
	logger.LogEndpointCall("User", "GET")
	w.Header().Set("Content-Type", "application/json")

	// retrieve user from request
	var user models.User
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &user)
	if err != nil {
		logger.LogError("Error while reading request body", nil)
	}

	collection := database.GetUsers()
	var result models.User
	var res models.ResponseResult

	dbUser := collection.FindOne(context.TODO(), bson.D{{"identifier", user.Identifier}}).Decode(&result)

	if dbUser != nil {
		res.Error = "Invalid identifier"
		json.NewEncoder(w).Encode(res)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(user.Password))

	if err != nil {
		res.Error = "Invalid password"
		json.NewEncoder(w).Encode(res)
		return
	}

	ttl := 1 * time.Hour
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"identifier": result.Identifier,
		"exp":        time.Now().UTC().Add(ttl).Unix(),
	})

	tokenString, err := token.SignedString([]byte("secret"))

	if err != nil {
		res.Error = "Error while generating token,Try again"
		json.NewEncoder(w).Encode(res)
		return
	}

	result.Token = tokenString
	res.Result = result.Token

	json.NewEncoder(w).Encode(res)
}
