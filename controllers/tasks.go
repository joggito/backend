package controllers

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	dbconf "etiennepericat/myruns/server/config/database"
	"etiennepericat/myruns/server/helpers"
	"etiennepericat/myruns/server/logger"
	"etiennepericat/myruns/server/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GetTasks return all the tasks in the database for the connected user
func GetTasks(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	var res models.ResponseResult
	authenticated, user, err := helpers.AuthenticateUserWithToken(r)

	if err != nil {
		res.Error = err.Error()
	}

	// if user is authenticated
	if authenticated && user != nil {

		// retrieve tasks collection from db
		tasks := dbconf.GetTasks()
		ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)

		// retrieve "all tasks" of the user
		cursor, err := tasks.Find(ctx, bson.M{"userid": user.ID})
		if err != nil {
			logger.LogError("error retrieving all the tasks", err)
		}

		var resultTable []models.Task
		for cursor.Next(ctx) {

			var dbTask models.Task
			err := cursor.Decode(&dbTask)
			if err != nil {
				logger.LogError("error decoding one task", err)
			}

			// write each task during each loop
			resultTable = append(resultTable, dbTask)
		}
		cursor.Close(ctx)
		res.Result = resultTable
	}

	json.NewEncoder(w).Encode(res)
}

// PostTask create a new task for the connected user
func PostTask(w http.ResponseWriter, r *http.Request) {

	// apply headers
	w.Header().Set("Content-type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var res models.ResponseResult

	authenticated, user, err := helpers.AuthenticateUserWithToken(r)

	if err != nil {
		res.Error = err.Error()
	}

	// if user is authenticated
	if authenticated && user != nil {

		// retrieve tasks collection from db
		ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
		tasks := dbconf.GetTasks()

		// read and parse the body
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logger.LogError("error reading task request body", err)
		}
		var task models.Task
		err = json.Unmarshal(body, &task)

		if err != nil {
			logger.LogError("error unmarshalling task", err)
		}

		// Instantiate a new task (date values)
		models.NewTask(&task, user.ID)

		// insert the newly created task
		res.Result, err = tasks.InsertOne(ctx, task)

		if err != nil {
			logger.LogError("error inserting a new task", err)
		}
	}

	// send back the inserted ID
	json.NewEncoder(w).Encode(res)
}

// PutTask : update the given task for the connected user
func PutTask(w http.ResponseWriter, r *http.Request) {
	// apply headers
	w.Header().Set("Content-type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var res models.ResponseResult
	authenticated, user, err := helpers.AuthenticateUserWithToken(r)

	logger.LogError("error authenticating the user", err)
	res.Error = err.Error()

	// if user is authenticated
	if authenticated && user != nil {

		// retrieve id from the request
		taskID := r.URL.Query().Get("id")
		ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
		tasks := dbconf.GetTasks()

		// retrieve the updated content from the body
		body, err := ioutil.ReadAll(r.Body)
		logger.LogError("error reading task request body", err)

		var updatedTask models.Task
		err = json.Unmarshal(body, &updatedTask)
		updatedTask.UserID = user.ID

		taskOID, err := primitive.ObjectIDFromHex(taskID)
		logger.LogError("Error while retrieving task for edit mode", err)

		_, err = tasks.UpdateOne(ctx, bson.M{"_id": taskOID, "userid": user.ID}, bson.M{"$set": updatedTask})
		logger.LogError("Error while updating task for edit mode", err)

		// update request result
		if err == nil {
			res.Result = updatedTask
		}
	}
	// send back the inserted ID
	json.NewEncoder(w).Encode(res)
}
