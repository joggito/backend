package logger

import (
	"log"
)
// LogError logs an error in the system
func LogError(message string, err error) {
	
	if err != nil {
		log.Println("-- ERROR : " + message + " __##")
		log.Println(err.Error())
	}
}

// LogInfo logs an info in the system
func LogInfo(message string) {
	log.Println("-- INFO : " + message + " __##")
}

// LogEndpointCall logs a endpoint call
func LogEndpointCall(name, method string) {
	log.Println("-- CALL : " + method + " " + name + " __##")
}