package config

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	"etiennepericat/myruns/server/logger"
)

var (
	databaseName   = "myruns"
	collectionTask = "tasks"
	collectionUser = "users"
	username       = "admin"
	password       = "myruns36&"
//	databaseURL    = "mongodb://" + username + ":" + password + "@db:27017"
	databaseURL    = "mongodb://" + username + ":" + password + "@localhost:27017"
	errorMessage   = "error connecting to mongo database"
)

var db *mongo.Database

// GetDatabase retrieve a pointer on the database
func getDatabase() *mongo.Database {
	if db == nil {
		db = initMongoConnection()
	}

	return db 
}

// GetTasks retrieve the collection "tasks"
func GetTasks() *mongo.Collection {
	return getDatabase().Collection(collectionTask)
}

// GetUsers retrieve the collection "users"
func GetUsers() *mongo.Collection {
	return getDatabase().Collection(collectionUser)
}

// initMongoConnection instantiate the connection with mongoDB database
func initMongoConnection() *mongo.Database {

	// create a new client
	client, err := mongo.NewClient(options.Client().ApplyURI(databaseURL))
	logger.LogError("1 : ", err)

	// connecting to database
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	logger.LogError("2 :", err)

	// confirm connection with a ping
	err = client.Ping(ctx, readpref.Primary())
	logger.LogError("3 :", err)

	// open the databse
	return client.Database(databaseName)
}
