package config

import (
	"etiennepericat/myruns/server/controllers"

	"github.com/gorilla/mux"
)

// InitializeRouter ...
func InitializeRouter() *mux.Router {
	// StrictSlash is true => redirect /cars/ to /cars
	router := mux.NewRouter().StrictSlash(true)

	// user endpoints
	router.Methods("POST").Path("/register").Name("Register").HandlerFunc(controllers.PostRegisterUser)
	router.Methods("GET").Path("/login").Name("Login").HandlerFunc(controllers.GetLoginUser)
	router.Methods("POST").Path("/resetpwd").Name("ResetPwd").HandlerFunc(controllers.PostResetPassword)

	// tasks endpoints
	router.Methods("GET").Path("/tasks").Name("Index").HandlerFunc(controllers.GetTasks)
	router.Methods("POST").Path("/tasks").Name("Create").HandlerFunc(controllers.PostTask)
	router.Methods("PUT").Path("/tasks/{id}").Name("Update").HandlerFunc(controllers.PutTask)
	return router
}
